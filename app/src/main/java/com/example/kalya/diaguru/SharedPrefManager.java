package com.example.kalya.diaguru;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kalya on 21-Mar-18.
 */

public class SharedPrefManager {
    private static final String SHARED_PREF_NAME="dhjbsjhfbsfuhbwfhwebfwh";
    private static final String KEY_ACCESS_TOKEN="token";

    private static Context mCtx;
    private static SharedPrefManager mInstance;

    private SharedPrefManager(Context context){
        mCtx=context;
    }

    public static synchronized SharedPrefManager getInstance(Context context){
        if(mInstance == null)
            mInstance = new SharedPrefManager(context);
        return mInstance;
    }

    public boolean storeToken(String token){
        SharedPreferences sharedpreferences =mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedpreferences.edit();
        editor.putString(KEY_ACCESS_TOKEN,token);
        editor.apply();
        return true;

    }

    public String getToken(){
        SharedPreferences sharedpreferences =mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedpreferences.getString(KEY_ACCESS_TOKEN,null);

    }
}
