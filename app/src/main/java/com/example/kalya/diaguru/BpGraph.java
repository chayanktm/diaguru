package com.example.kalya.diaguru;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class BpGraph extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUsers;
    private final String secondreq="Secondary_requirements";
    private String year="",month="";
    private Calendar now = Calendar.getInstance();
    private ArrayList<secondary_util_retrieve_firebase> a;
    private Spinner sp;
    private LineGraphSeries<DataPoint> series,series2,series3,series4;
    private ArrayAdapter<CharSequence> adapter;

    @Override
      protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bp_graph);
        sp= (Spinner) findViewById(R.id.spinner);

        adapter= ArrayAdapter.createFromResource(this,R.array.months,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);

        sp.setSelection(now.get(Calendar.MONTH));


        year=String.valueOf(now.get(Calendar.YEAR));



        int mon=(now.get(Calendar.MONTH)+1);
        if(mon<10){
            month= "0"+String.valueOf(mon);
        }else{
            month= String.valueOf(mon);
        }


        Produce_Graph();




        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {



                if(i+1<10){
                    month= "0"+String.valueOf(i+1);
                }else{
                    month= String.valueOf(i+1);
                }
                year=String.valueOf(now.get(Calendar.YEAR));

                checkdatabase();



            }

            private void checkdatabase() {
                mAuth = FirebaseAuth.getInstance();
                a=new ArrayList<>();



                String user_id=getIncomingIntent();
                if(user_id.equals("")){
                    user_id=mAuth.getCurrentUser().getUid();
                }

                Log.d("month : : : :",month);
                mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

                mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            // TODO: handle the post

                            HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                            String day=postSnapshot.getKey();
                            String sugar_level=b.get("Sugar_level");
                            String Food_intake=b.get("Food_intake");
                            String BP_level_sp=b.get("BP_level_sp");
                            String BP_level_dp=b.get("BP_level_dp");
                            String Insulin_dosage=b.get("Insulin_dosage");
                            secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,"",Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                            a.add(c);
                            Log.d("USER", ":stuff is there ");

                        }
                        try {

                            series3.resetData(getDataPointcomb());
                        }catch (Exception e){
                            Log.d("USER", ":some error  00 "+ e);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {



            }
        });


    }

    private void Produce_Graph(){

        mAuth = FirebaseAuth.getInstance();
        a=new ArrayList<>();



        String user_id=getIncomingIntent();
        if(user_id.equals("")){
            user_id=mAuth.getCurrentUser().getUid();
        }

        Log.d("month : : : :",month);
        mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

        mDatabaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    // TODO: handle the post

                    HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                    String day=postSnapshot.getKey();
                    String sugar_level=b.get("Sugar_level");
                    String Food_intake=b.get("Food_intake");
                    String BP_level_sp=b.get("BP_level_sp");
                    String BP_level_dp=b.get("BP_level_dp");
                    String Insulin_dosage=b.get("Insulin_dosage");
                    secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,"",Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                    a.add(c);
                    Log.d("USER", ":stuff is there ");

                }

                check();




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    private void check(){

        /*
        GraphView graph = (GraphView) findViewById(R.id.graph);
        GraphView graph_sp = (GraphView) findViewById(R.id.graph2);
        DataPoint[]  dp=getDataPoint();
        DataPoint[]  sp=getDataPointsp();
        series = new LineGraphSeries<>(dp);
        series2=new LineGraphSeries<>(sp);
        graph.addSeries(series);
        graph_sp.addSeries(series2);
        series.setTitle("dp urve");
        series.setColor(Color.RED);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);
        graph.getViewport().setMinX(1);
        graph.getViewport().setMaxX(30);
        graph.getViewport().setMinY(30);
        graph.getViewport().setMaxY(100);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScrollableY(true);
        GridLabelRenderer gridLabel = graph.getGridLabelRenderer();
        gridLabel.setHorizontalAxisTitle("Days");
        gridLabel.setPadding(20);
        gridLabel.setVerticalAxisTitle("DP Level");
        gridLabel.setPadding(20);
        series2.setTitle("sp Curve ");
        series2.setColor(Color.RED);
        series2.setDrawDataPoints(true);
        series2.setDataPointsRadius(10);
        series2.setThickness(8);
        graph_sp.getViewport().setMinX(1);
        graph_sp.getViewport().setMaxX(30);
        graph_sp.getViewport().setMinY(50);
        graph_sp.getViewport().setMaxY(200);
        graph_sp.getViewport().setYAxisBoundsManual(true);
        graph_sp.getViewport().setXAxisBoundsManual(true);
        graph_sp.getViewport().setScalable(true);
        graph_sp.getViewport().setScalableY(true);
        graph_sp.getViewport().setScrollable(true);
        graph_sp.getViewport().setScrollableY(true);
        GridLabelRenderer gridLabel1 = graph_sp.getGridLabelRenderer();
        gridLabel1.setHorizontalAxisTitle("Days");
        gridLabel1.setPadding(20);
        gridLabel1.setVerticalAxisTitle("SP Level");
        gridLabel1.setPadding(20);

        */
        GraphView graph_comb = (GraphView) findViewById(R.id.graph3);
        DataPoint[]  comb=getDataPointcomb();
        DataPoint[]  ideal=getDataPointIdeal();
        series3=new LineGraphSeries<>(comb);
        series4=new LineGraphSeries<>(ideal);
        graph_comb.addSeries(series3);
        graph_comb.addSeries(series4);
        series3.setTitle("combo curve");
        series3.setColor(Color.RED);
        series3.setDrawDataPoints(true);
        series3.setDataPointsRadius(10);
        series3.setThickness(8);

        series4.setColor(Color.GREEN);
        series4.setDrawDataPoints(true);
        series4.setDataPointsRadius(5);
        series4.setThickness(5);
       graph_comb.getViewport().setMinY(0);
       graph_comb.getViewport().setMaxY(7);
        graph_comb.getViewport().setMinX(1);
        graph_comb.getViewport().setMaxX(30);
       graph_comb.getViewport().setYAxisBoundsManual(true);
        graph_comb.getViewport().setScrollable(true);
        graph_comb.getViewport().setScalable(true);

        GridLabelRenderer gridLabel2 = graph_comb.getGridLabelRenderer();

        gridLabel2.setHorizontalAxisTitle("Days");
        gridLabel2.setPadding(20);
        gridLabel2.setVerticalAxisTitle("Type of BP");
        gridLabel2.setPadding(20);
        graph_comb.getGridLabelRenderer().setNumVerticalLabels(7);

    }
    private DataPoint[] getDataPoint(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= Integer.parseInt(a.get(i).getBP_level_dp());
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":dp  ponts");

        return dp;
    }
    private DataPoint[] getDataPointsp(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= Integer.parseInt(a.get(i).getBP_level_sp());
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":sp  ponts");

        return dp;
    }

    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }

    private DataPoint[] getDataPointcomb(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y2= Integer.parseInt(a.get(i).getBP_level_sp());
            int y1= Integer.parseInt(a.get(i).getBP_level_dp());
            int y=0;


            if(y1<=130 && y2<=200) y=6;   //  hypertensive
            if(y1<=120 && y2<=180) y=5;  //  hypertension step 2
            if(y1<=90 && y2<=140) y=4;  // hypertension step1
            if(y1<=80 && y2<=130) y=3;  //elevated
            if(y1<=80 && y2<=120) y=2;  //normal bp
            if(y1<=60 && y2<=90) y=1; //hypertension



            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
           Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":assigned data to datapoints fro combo");

        return dp;
    }


    private DataPoint[] getDataPointIdeal(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {
            int y=2;
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":assigned data to datapoints fro ideal graph");

        return dp;
    }

}
