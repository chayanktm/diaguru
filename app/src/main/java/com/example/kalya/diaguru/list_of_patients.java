package com.example.kalya.diaguru;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class list_of_patients extends AppCompatActivity {


    private DatabaseReference myRef ;
    private List<FireModel> list;
    private RecyclerView recycle;
    private Button view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_patients);
        view = (Button) findViewById(R.id.view);
        recycle = (RecyclerView) findViewById(R.id.recycle);

        myRef = FirebaseDatabase.getInstance().getReference().child("Doctor").child("Users");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                list = new ArrayList<FireModel>();
                for(DataSnapshot postSnapshot :dataSnapshot.getChildren()){

                    HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                    String uid=postSnapshot.getKey();
                    FireModel fire = new FireModel();
                    String name =b.get("name");
                    String email = b.get("email");
                    String phone=b.get("phone number");
                    fire.setName(name);
                    fire.setEmail(email);
                    fire.setUid(uid);
                    fire.setPhone(phone);
                    list.add(fire);

                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                RecyclerAdapter recyclerAdapter = new RecyclerAdapter(list,list_of_patients.this);
                RecyclerView.LayoutManager recyce = new GridLayoutManager(list_of_patients.this,2);
                /// RecyclerView.LayoutManager recyce = new LinearLayoutManager(MainActivity.this);
                // recycle.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
                recycle.setLayoutManager(recyce);
                recycle.setItemAnimator( new DefaultItemAnimator());
                recycle.setAdapter(recyclerAdapter);




            }
        });




    }
}
