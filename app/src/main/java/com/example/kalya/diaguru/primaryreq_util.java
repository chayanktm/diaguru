package com.example.kalya.diaguru;

/**
 * Created by kalya on 13-Mar-18.
 */

public class primaryreq_util {
    public String fullname,age,gender,dob,occupation,address,height,weight,bmi,type_of_diabetes,duration_of_diabetes,type_of_treatment,hb1ac,mobile_number;

    public primaryreq_util(){
        /// default required
    }
    public primaryreq_util( String name,String age, String gender, String dob, String occupation, String address, String height, String weight, String bmi, String type_of_diabetes, String duration_of_diabetes, String type_of_treatment, String hb1ac, String mobile_number) {
        this.fullname = name;
        this.age = age;
        this.gender = gender;
        this.dob = dob;
        this.occupation = occupation;
        this.address = address;
        this.height = height;
        this.weight = weight;
        this.bmi = bmi;
        this.type_of_diabetes = type_of_diabetes;
        this.duration_of_diabetes = duration_of_diabetes;
        this.type_of_treatment = type_of_treatment;
        this.hb1ac = hb1ac;
        this.mobile_number = mobile_number;
    }
}
