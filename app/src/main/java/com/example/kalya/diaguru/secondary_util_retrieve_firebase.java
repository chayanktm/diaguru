package com.example.kalya.diaguru;

/**
 * Created by kalya on 20-Mar-18.
 */

public class secondary_util_retrieve_firebase {

    private String day,Sugar_level,Sugar_level_rand,BP_level_sp,BP_level_dp,Insulin_dosage,Food_intake;

    public secondary_util_retrieve_firebase(String day,String sugar_level,String Sugar_level_rand, String food_intake, String bp_level_dp,String bp_level_sp,  String insulin_dosage) {
        this.day = day;
        this.Sugar_level = sugar_level;
        this.Sugar_level_rand = Sugar_level_rand;
        this.BP_level_dp = bp_level_dp;
        this.BP_level_sp = bp_level_sp;
        this.Insulin_dosage = insulin_dosage;
        this.Food_intake = food_intake;
    }

    public String getDay() {
        return day;
    }

    public String getSugar_level() {
        return Sugar_level;
    }



    public String getInsulin_dosage() {
        return Insulin_dosage;
    }

    public String getFood_intake() {
        return Food_intake;
    }

    public String getBP_level_sp() {
        return BP_level_sp;
    }

    public void setBP_level_sp(String BP_level_sp) {
        this.BP_level_sp = BP_level_sp;
    }

    public String getBP_level_dp() {
        return BP_level_dp;
    }

    public void setBP_level_dp(String BP_level_dp) {
        this.BP_level_dp = BP_level_dp;
    }

    public String getSugar_level_rand() {
        return Sugar_level_rand;
    }

    public void setSugar_level_rand(String sugar_level_rand) {
        Sugar_level_rand = sugar_level_rand;
    }
}
