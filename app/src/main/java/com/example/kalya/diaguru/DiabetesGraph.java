package com.example.kalya.diaguru;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DiabetesGraph extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUsers;
    private final String secondreq="Secondary_requirements";
    private String year="",month="";
    Calendar now = Calendar.getInstance();
    private ArrayList<secondary_util_retrieve_firebase> a;
    private Spinner sp;
    private LineGraphSeries<DataPoint> series,series1,series3,series4,series5,series6;
    //make string array
    //private String mon[]={"January","February","March","April","May","June","July","August","September","October","November","December"};

    // define array adpater
    private ArrayAdapter<CharSequence> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diabetes_graph);

        sp= (Spinner) findViewById(R.id.spinner);

        adapter=ArrayAdapter.createFromResource(this,R.array.months,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);

        sp.setSelection(now.get(Calendar.MONTH));


        year=String.valueOf(now.get(Calendar.YEAR));

               int mon=(now.get(Calendar.MONTH)+1);
                if(mon<10){
                    month= "0"+String.valueOf(mon);
                }else{
                    month= String.valueOf(mon);
                }


        Produce_Graph();




        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {


                year=String.valueOf(now.get(Calendar.YEAR));
                if(i+1<10){
                    month= "0"+String.valueOf(i+1);
                }else{
                    month= String.valueOf(i+1);
                }

                checkdatabase();



            }

            private void checkdatabase() {
                mAuth = FirebaseAuth.getInstance();
                a=new ArrayList<>();



                String user_id=getIncomingIntent();
                if(user_id.equals("")){
                    user_id=mAuth.getCurrentUser().getUid();
                }

                Log.d("month : : : :",month);
                mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

                mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            // TODO: handle the post

                            HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                            String day=postSnapshot.getKey();
                            String sugar_level=b.get("Sugar_level");
                            String sugar_level_rand=b.get("Sugar_level_random");
                            String Food_intake=b.get("Food_intake");
                            String BP_level_sp=b.get("BP_level_sp");
                            String BP_level_dp=b.get("BP_level_dp");
                            String Insulin_dosage=b.get("Insulin_dosage");
                            secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,sugar_level_rand,Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                            a.add(c);
                            Log.d("USER", ":stuff is there ");

                        }

                        try{
                            series.resetData(getDataPoint());
                            series1.resetData(getrandDatapoint());
                            series3.resetData(getidealdatapoint1());
                            series4.resetData(getidealdatapoint2());
                            series5.resetData(getidealdatapointrand1());
                            series6.resetData(getidealdatapointrand2());


                        }catch (Exception e)
                        {
                            Log.d("USER", ":some error  00 "+ e);
                        }



                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {



            }
        });


    }

    private void Produce_Graph(){

        mAuth = FirebaseAuth.getInstance();
        a=new ArrayList<>();



        String user_id=getIncomingIntent();
        if(user_id.equals("")){
            user_id=mAuth.getCurrentUser().getUid();
        }

        Log.d("month : : : :",month);
        mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

        mDatabaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    // TODO: handle the post

                    HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                    String day=postSnapshot.getKey();
                    String sugar_level=b.get("Sugar_level");
                    String sugar_level_rand=b.get("Sugar_level_random");
                    String Food_intake=b.get("Food_intake");
                    String BP_level_sp=b.get("BP_level_sp");
                    String BP_level_dp=b.get("BP_level_dp");
                    String Insulin_dosage=b.get("Insulin_dosage");
                    secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,sugar_level_rand,Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                    a.add(c);
                    Log.d("USER", ":stuff is there ");

                }

                check();




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    private void check(){
        GraphView graph = (GraphView) findViewById(R.id.graph);
        GraphView graph1 = (GraphView) findViewById(R.id.graph1);

        DataPoint[]  dp=getDataPoint();
        DataPoint[]  dp_rand=getrandDatapoint();

        series = new LineGraphSeries<>(dp);
        series1 = new LineGraphSeries<>(dp_rand);
        series3 =new LineGraphSeries<>(getidealdatapoint1());
        series4 =new LineGraphSeries<>(getidealdatapoint2());
        series5 =new LineGraphSeries<>(getidealdatapointrand1());
        series6 =new LineGraphSeries<>(getidealdatapointrand2());

        graph.addSeries(series);
        graph.addSeries(series3);
        graph.addSeries(series4);


        graph1.addSeries(series1);
        graph1.addSeries(series5);
        graph1.addSeries(series6);

        graph.getViewport().setMinX(1);
        graph.getViewport().setMaxX(30);
        graph.getViewport().setMinY(1);
        graph.getViewport().setMaxY(500);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);
        series.setTitle("Random Curve 1");
        series.setColor(Color.RED);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(DiabetesGraph.this, "Series1: On Data Point clicked: "+dataPoint, Toast.LENGTH_SHORT).show();
            }
        });


        series3.setTitle("Random Curve 1");
        series3.setColor(Color.GREEN);
        series3.setDrawDataPoints(true);
        series3.setDataPointsRadius(5);
        series3.setThickness(5);

        series4.setTitle("Random Curve 1");
        series4.setColor(Color.GREEN);
        series4.setDrawDataPoints(true);
        series4.setDataPointsRadius(5);
        series4.setThickness(5);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScrollableY(true);

        GridLabelRenderer gridLabel = graph.getGridLabelRenderer();
        gridLabel.setHorizontalAxisTitle("Days");
        gridLabel.setPadding(20);
        gridLabel.setVerticalAxisTitle("Sugar Level");
        gridLabel.setPadding(20);





        graph1.getViewport().setMinX(1);
        graph1.getViewport().setMaxX(30);
        graph1.getViewport().setMinY(1);
        graph1.getViewport().setMaxY(500);

        graph1.getViewport().setYAxisBoundsManual(true);
        graph1.getViewport().setXAxisBoundsManual(true);
        series1.setTitle("Random Curve 1");
        series1.setColor(Color.RED);
        series1.setDrawDataPoints(true);
        series1.setDataPointsRadius(10);
        series1.setThickness(8);

        series5.setTitle("Random Curve 1");
        series5.setColor(Color.GREEN);
        series5.setDrawDataPoints(true);
        series5.setDataPointsRadius(5);
        series5.setThickness(5);

        series6.setTitle("Random Curve 1");
        series6.setColor(Color.GREEN);
        series6.setDrawDataPoints(true);
        series6.setDataPointsRadius(5);
        series6.setThickness(5);
        graph1.getViewport().setScalable(true);
        graph1.getViewport().setScalableY(true);
        graph1.getViewport().setScrollable(true);
        graph1.getViewport().setScrollableY(true);

        GridLabelRenderer gridLabel1 = graph1.getGridLabelRenderer();
        gridLabel1.setHorizontalAxisTitle("Days");
        gridLabel1.setPadding(20);
        gridLabel1.setVerticalAxisTitle("Sugar Level Random");
        gridLabel1.setPadding(20);
    }

    private DataPoint[] getrandDatapoint() {
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= Integer.parseInt(a.get(i).getSugar_level_rand());
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }

    private DataPoint[] getDataPoint(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= Integer.parseInt(a.get(i).getSugar_level());
           int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }

    private DataPoint[] getidealdatapoint1(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y=80;
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }

    private DataPoint[] getidealdatapoint2(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y=100;
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }

    private DataPoint[] getidealdatapointrand1(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= 170;
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }
    private DataPoint[] getidealdatapointrand2(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= 200;
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }




    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }

}
