package com.example.kalya.diaguru;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kalya on 19-Mar-18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="Patients.db";
    public static final String TABLE_NAME="secondary_requirements";
    //public static final String COL_1="ID";
    public static final String COL_2="DATE";
    public static final String COL_3="SUGAR";
    public static final String COL_4="BP";
    public static final String COL_5="INSULIN";
    public static final String COL_6="FOODINTAKE";



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME +" ("+COL_2+" DATE PRIMARY KEY,"+COL_3+" INTEGER ,"+COL_4+" INTEGER,"+COL_5+"  INTEGER ,"+COL_6+" INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String date,String sugar,String bp,String insulin,String foodintake)
    {
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(COL_2,date);
        contentValues.put(COL_3,sugar);
        contentValues.put(COL_4,bp);
        contentValues.put(COL_5,insulin);
        contentValues.put(COL_6,foodintake);
        long result = db.insert(TABLE_NAME,null,contentValues);

        return (result!=-1);

    }

    public Cursor getAllData(){
        SQLiteDatabase db =this.getWritableDatabase();
        Cursor res =db.rawQuery("select * from " + TABLE_NAME,null);
        return res;
    }

    public boolean updateData(String date,String sugar,String bp,String insulin,String foodintake){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        //contentValues.put(COL_1,id);
        contentValues.put(COL_2,date);
        contentValues.put(COL_3,sugar);
        contentValues.put(COL_4,bp);
        contentValues.put(COL_5,insulin);
        contentValues.put(COL_6,foodintake);
        db.update(TABLE_NAME,contentValues,COL_2+"= ?",new String[]{ date} );
        return true;
    }

    public Integer deletedata(String date){
        SQLiteDatabase db =this.getWritableDatabase();
        return db.delete(TABLE_NAME,COL_2+"= ?",new String[] { date});
    }
}
