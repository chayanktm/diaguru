package com.example.kalya.diaguru;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class Prescription extends AppCompatActivity {

    private EditText edtxt_presc;
    private TextView txt_presc;
    private Button update;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);



        edtxt_presc= (EditText) findViewById(R.id.edtxt_post_presc);
        txt_presc = (TextView) findViewById(R.id.txt_presc);
        update = (Button) findViewById(R.id.btn_update);

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Doctor/Users/"+getIncomingIntent());
        mAuth = FirebaseAuth.getInstance();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild("Prescription")) {


                    HashMap<String, String> b = (HashMap<String, String>) dataSnapshot.getValue();
                    String presc = b.get("Prescription");
                    txt_presc.setText(presc);


                }else{
                    txt_presc.setText("Doctor has given no prescription");
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mDatabase.child("Prescription").setValue(edtxt_presc.getText().toString());
            }
        });








    }

    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }
}
