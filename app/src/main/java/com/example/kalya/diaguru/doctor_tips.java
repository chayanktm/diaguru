package com.example.kalya.diaguru;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class doctor_tips extends AppCompatActivity {

    private EditText edtxt_postanswer1;
    private TextView txt_question1,txt_answer1;
    private Button update;

    private DatabaseReference mDatabase,mDatabaseAnswer;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_tips);


        edtxt_postanswer1= (EditText) findViewById(R.id.edtxt_postanswer);
        txt_question1 =(TextView) findViewById(R.id.txt_question);
        txt_answer1 =(TextView) findViewById(R.id.txt_answer);

        update= (Button) findViewById(R.id.btn_update) ;


        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user=mAuth.getCurrentUser();

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Doctor/Users/"+getIncomingIntent());
        mDatabaseAnswer =FirebaseDatabase.getInstance().getReference().child("Doctor/Users/"+getIncomingIntent());


        mDatabaseAnswer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild("tipa")) {


                    HashMap<String, String> b = (HashMap<String, String>) dataSnapshot.getValue();
                    String presc = b.get("tipa");

                    txt_answer1.setText(presc);


                }else{

                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild("tipq")) {


                    HashMap<String, String> b = (HashMap<String, String>) dataSnapshot.getValue();
                    String presc = b.get("tipq");
                    txt_question1.setText(presc);



                }else{
                    txt_question1.setText("You haven't asked any questions.please do ask. :)");
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("tipa").setValue(edtxt_postanswer1.getText().toString());

            }
        });
    }


    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }
}
