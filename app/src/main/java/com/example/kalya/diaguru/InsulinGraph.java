package com.example.kalya.diaguru;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class InsulinGraph extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUsers;
    private final String secondreq="Secondary_requirements";
    private String year="",month="";
    Calendar now = Calendar.getInstance();
    private ArrayList<secondary_util_retrieve_firebase> a;
    private Spinner sp;
    private LineGraphSeries<DataPoint> series;
    private ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insulin_graph);
        sp= (Spinner) findViewById(R.id.spinner);

        adapter= ArrayAdapter.createFromResource(this,R.array.months,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);

        sp.setSelection(now.get(Calendar.MONTH));


        year=String.valueOf(now.get(Calendar.YEAR));
        int mon=(now.get(Calendar.MONTH)+1);
        if(mon<10){
            month= "0"+String.valueOf(mon);
        }else{
            month= String.valueOf(mon);
        }


        Produce_Graph();




        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {



                year=String.valueOf(now.get(Calendar.YEAR));
                if(i+1<10){
                    month= "0"+String.valueOf(i+1);
                }else{
                    month= String.valueOf(i+1);
                }

                checkdatabase();



            }

            private void checkdatabase() {
                mAuth = FirebaseAuth.getInstance();
                a=new ArrayList<>();



                String user_id=getIncomingIntent();
                if(user_id.equals("")){
                    user_id=mAuth.getCurrentUser().getUid();
                }

                Log.d("month : : : :",month);
                mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

                mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            // TODO: handle the post

                            HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                            String day=postSnapshot.getKey();
                            String sugar_level=b.get("Sugar_level");
                            String Food_intake=b.get("Food_intake");
                            String BP_level_sp=b.get("BP_level_sp");
                            String BP_level_dp=b.get("BP_level_dp");
                            String Insulin_dosage=b.get("Insulin_dosage");
                            secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,"",Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                            a.add(c);
                            Log.d("USER", ":stuff is there ");

                        }

                        try{
                            series.resetData(getDataPoint());
                        }catch (Exception e)
                        {
                            Log.d("USER", ":some error  00 "+ e);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {



            }
        });


    }

    private void Produce_Graph(){

        mAuth = FirebaseAuth.getInstance();
        a=new ArrayList<>();



        String user_id=getIncomingIntent();
        if(user_id.equals("")){
            user_id=mAuth.getCurrentUser().getUid();
        }

        Log.d("month : : : :",month);
        mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child(secondreq).child(year).child(month);

        mDatabaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    // TODO: handle the post

                    HashMap<String,String> b = (HashMap<String,String>)postSnapshot.getValue();
                    String day=postSnapshot.getKey();
                    String sugar_level=b.get("Sugar_level");
                    String Food_intake=b.get("Food_intake");
                    String BP_level_sp=b.get("BP_level_sp");
                    String BP_level_dp=b.get("BP_level_dp");
                    String Insulin_dosage=b.get("Insulin_dosage");
                    secondary_util_retrieve_firebase c=new secondary_util_retrieve_firebase(day,sugar_level,"",Food_intake,BP_level_dp,BP_level_sp,Insulin_dosage);
                    a.add(c);
                    Log.d("USER", ":stuff is there ");

                }

                check();




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    private void check(){
        GraphView graph = (GraphView) findViewById(R.id.graph);

        DataPoint[]  dp=getDataPoint();
        series = new LineGraphSeries<>(dp);
        graph.addSeries(series);

        graph.getViewport().setMinX(1);
        graph.getViewport().setMaxX(30);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(25);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);
        series.setTitle("Random Curve 1");
        series.setColor(Color.RED);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScrollableY(true);
        GridLabelRenderer gridLabel = graph.getGridLabelRenderer();
        gridLabel.setHorizontalAxisTitle("Days");
        gridLabel.setPadding(20);
        gridLabel.setVerticalAxisTitle("Insulin Level");
        gridLabel.setPadding(20);
    }
    private DataPoint[] getDataPoint(){
        DataPoint[]  dp=new DataPoint[a.size()];
        for(int i=0;i<a.size();i++)
        {  int y= Integer.parseInt(a.get(i).getInsulin_dosage());
            int x= Integer.parseInt(a.get(i).getDay());
            dp[i]=new DataPoint(x,y);
            Log.d("USER", ":"+x+" : "+y);
        }
        Log.d("USER", ":hello no loop");

        return dp;
    }

    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }

}
