package com.example.kalya.diaguru;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

import static com.example.kalya.diaguru.NetworkCheck.isNetworkAvailable;

public class Homescreen extends AppCompatActivity {
    Button logout,daily_inputs,reports,prescriptions,tips,foodchart,setreminder;
    TextView name;
    private DatabaseReference mDatabase,mDatabaseUsers;
    private FirebaseAuth mAuth;

    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (mHandler != null) { mHandler.removeCallbacks(mRunnable); }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(mRunnable, 2000);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        Log.d("myfcmtokenshared",SharedPrefManager.getInstance(Homescreen.this).getToken());


        mAuth = FirebaseAuth.getInstance();

        String uid= mAuth.getCurrentUser().getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Doctor").child("Users").child(uid).child("name");
        mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users");


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                name=(TextView) findViewById(R.id.txt_name);

                name.append("\n"+(String) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        logout=(Button) findViewById(R.id.btn_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent out=new Intent(Homescreen.this,Signin.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);
                finish();
            }
        });

        daily_inputs=(Button) findViewById(R.id.btn_daily);
        daily_inputs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(Homescreen.this,Secondary_req.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);
            }
        });


        reports=(Button) findViewById(R.id.btn_reports);
        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(Homescreen.this,ReportType.class);
                startActivity(out);
            }
        });


        prescriptions=(Button) findViewById(R.id.btn_prescription);
        prescriptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Homescreen.this,patients_prescription.class);
                startActivity(in);

            }
        });

        foodchart=(Button) findViewById(R.id.btn_foodchart);
        foodchart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://drive.google.com/drive/folders/1slHzx72GRIrl2WdykBk-jpUrDT1Qpgs4";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        tips=(Button) findViewById(R.id.btn_tips);
        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent( Homescreen.this,patient_tips.class);

                startActivity(i);
            }
        });

/*
        setreminder=(Button) findViewById(R.id.btn_set_reminder);
        setreminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           Calendar calendar =Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY,22);
                calendar.set(Calendar.MINUTE,9);
                calendar.set(Calendar.SECOND,40);

                Toast.makeText(Homescreen.this, "reminders set",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(),Notification_reciever.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),100,intent,PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);


            }
        });
        */

    }

    @Override
    public void onStart() {
        super.onStart();

        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"No Internet connection",Toast.LENGTH_LONG).show();
            finish(); //Calling this method to close this activity when internet is not available.
        }
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser= mAuth.getCurrentUser();
        checkuserexist(currentUser);
    }

    private void checkuserexist(FirebaseUser user) {

        if (user != null) {
            final String user_id=user.getUid();

                mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(user_id)) {



                        } else {
                            Intent in = new Intent(Homescreen.this, Signin.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(in);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




        } else {

            Intent in = new Intent(Homescreen.this,Signin.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);


        }
    }

}
