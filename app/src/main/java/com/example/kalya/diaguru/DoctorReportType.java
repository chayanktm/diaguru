package com.example.kalya.diaguru;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static java.net.Proxy.Type.HTTP;

public class DoctorReportType extends AppCompatActivity {

    Button diabetes,bp,insulin,foodintake,email,phone,presc,profile,tips;
    TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_report_type);


        name=(TextView) findViewById(R.id.txt_name);
        name.append(getIncomingIntentName());


        profile=(Button) findViewById(R.id.btn_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent out=new Intent(DoctorReportType.this,Patients_Profile_for_doc.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);

            }
        });


        presc=(Button) findViewById(R.id.btn_prescription);
        presc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent out=new Intent(DoctorReportType.this,Prescription.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);

            }
        });


        diabetes=(Button) findViewById(R.id.btn_diabetes);
        diabetes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent out=new Intent(DoctorReportType.this,DiabetesGraph.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);

            }
        });

        bp=(Button) findViewById(R.id.btn_bp);
        bp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(DoctorReportType.this,BpGraph.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);
            }
        });

        insulin=(Button) findViewById(R.id.btn_insulin);
        insulin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(DoctorReportType.this,InsulinGraph.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);
            }
        });

        tips=(Button) findViewById(R.id.btn_tips);
        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(DoctorReportType.this,doctor_tips.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);
            }
        });



        /*foodintake=(Button) findViewById(R.id.btn_foodintake);
        foodintake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(DoctorReportType.this,FoodIntake.class);
                out.putExtra("uid",getIncomingIntent());

                startActivity(out);
            }
        });

        */

        email=(Button) findViewById(R.id.btn_email);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent emailIntent = new Intent(Intent.ACTION_SEND);
// The intent does not have a URI, so declare the "text/plain" MIME type
                emailIntent.setType("plain/text");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {getIncomingIntentEmail()}); // recipients
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Health Report");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "hello "+getIncomingIntentName()+"\n this is to inform you that your health records are not in the normal range. please make sure you are maintaining your sugar/bp\n Thankyou \n dr.XYZ");

                startActivity(emailIntent);
            }
        });

        phone=(Button) findViewById(R.id.btn_phone);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri number = Uri.parse("tel:"+getIncomingIntentPhone());
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
            }
        });


    }

    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

             uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }

    private String getIncomingIntentEmail()
    {
        Log.d("check here","getting incoming inetent");
        String email="";
        if(getIntent().hasExtra("email")){
            Log.d("check here","her is email");

            email=getIntent().getStringExtra("email");


        }
        return email;
    }

    private String getIncomingIntentName()
    {
        Log.d("check here","getting incoming inetent");
        String name="";
        if(getIntent().hasExtra("name")){
            Log.d("check here","her is name");

            name=getIntent().getStringExtra("name");


        }
        return name;
    }

    private String getIncomingIntentPhone()
    {
        Log.d("check here","getting incoming inetent");
        String phone="";
        if(getIntent().hasExtra("name")){
            Log.d("check here","her is phone");

            phone=getIntent().getStringExtra("phone");


        }
        return phone;
    }
}
