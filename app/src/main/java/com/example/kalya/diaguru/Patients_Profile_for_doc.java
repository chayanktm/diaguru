package com.example.kalya.diaguru;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Patients_Profile_for_doc extends AppCompatActivity {

    private TextView name,age,gender,dob,occupation,address,height,tod,bmi,dod,hba1c,tot;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients__profile_for_doc);

        name=(TextView) findViewById(R.id.txt_fullname);
        age=(TextView) findViewById(R.id.txt_age);
        gender=(TextView) findViewById(R.id.txt_gender);
        dob=(TextView) findViewById(R.id.txt_dob);
        occupation=(TextView) findViewById(R.id.txt_occupation);
        address=(TextView) findViewById(R.id.txt_address);
        height=(TextView) findViewById(R.id.txt_height);
        tod=(TextView) findViewById(R.id.txt_tod);

        bmi=(TextView) findViewById(R.id.txt_bmi);
        dod=(TextView) findViewById(R.id.txt_dod);
        hba1c=(TextView) findViewById(R.id.txt_hba1c);

        tot=(TextView) findViewById(R.id.txt_tot);

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Users/"+getIncomingIntent()+"/Primary_Requirements");
        mAuth = FirebaseAuth.getInstance();


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                    HashMap<String, String> b = (HashMap<String, String>) dataSnapshot.getValue();



                CharSequence NAME =b.get("fullname");

                CharSequence AGE= "AGE : "+b.get("age");
                CharSequence GENDER= "GENDER : "+b.get("gender");
                CharSequence DOB= "DOB : "+b.get("dob");
                CharSequence OCCUPATION= "OCCUPATION : "+b.get("occupation");
                CharSequence ADDRESS= "ADDRESS : "+b.get("address");
                CharSequence HEIGHT= "HEIGHT : "+b.get("height");
                CharSequence TOD= "TOD : "+b.get("type_of_diabetes");
                CharSequence TOT= "TOT : "+b.get("type_of_treatment");

                CharSequence BMI= "BMI : "+b.get("bmi");
                CharSequence DOD= "DOD : "+b.get("duration_of_diabetes");
                CharSequence HB1AC= "HB1AC : "+b.get("hb1ac");


                Log.d("USER:  ", NAME.toString());

                    try {

                        name.setText(NAME);
                        age.setText(AGE);
                        gender.setText(GENDER);
                        dob.setText(DOB);
                        occupation.setText(OCCUPATION);
                        address.setText(ADDRESS);
                        height.setText(HEIGHT);
                        tod.setText(TOD);
                        tot.setText(TOT);
                        bmi.setText(BMI);
                        dod.setText(DOD);
                        hba1c.setText(HB1AC);
                    }catch (Exception e)
                    {

                        Log.d("USER:  ",e.toString());

                    }





            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });






    }

    private String getIncomingIntent(){
        Log.d("check here","getting incoming inetent");
        String uid="";
        if(getIntent().hasExtra("uid")){
            Log.d("check here","her is uid");

            uid=getIntent().getStringExtra("uid");


        }
        return uid;
    }
}
