package com.example.kalya.diaguru;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

import javax.microedition.khronos.egl.EGLDisplay;

public class Secondary_req extends AppCompatActivity {
    private String s="";
    private EditText sugar,bp_d,bp_s,insulin,food,date,sugar_random;
    private Button submit;
    Calendar now = Calendar.getInstance();
    TextView dateh;

    private Spinner sp;
    private ArrayAdapter<CharSequence> adapter;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary_req);

        sp= (Spinner) findViewById(R.id.spinner);

        adapter= ArrayAdapter.createFromResource(this,R.array.foodtype,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp.setAdapter(adapter);


        sugar=(EditText) findViewById(R.id.edtxt_sugar);
        bp_d=(EditText) findViewById(R.id.edtxt_bp_d);
        bp_s=(EditText) findViewById(R.id.edtxt_bp_s);
        insulin=(EditText) findViewById(R.id.edtxt_insulin);
        //food=(EditText) findViewById(R.id.edtxt_food);
       //3
        // date=(EditText) findViewById(R.id.edtxt_date);
        submit=(Button) findViewById(R.id.btn_submit);
        sugar_random=(EditText) findViewById(R.id.edtxt_sugar_after);
        dateh=(TextView) findViewById(R.id.txt_date);


        String year=String.valueOf(now.get(Calendar.YEAR));
        String month="",day="";

        int mon=(now.get(Calendar.MONTH)+1);
        if(mon<10){
            month= "0"+String.valueOf(mon);
        }else{
            month= String.valueOf(mon);
        }


        int day1=(now.get(Calendar.DATE));
        if(day1<10){
            day= "0"+String.valueOf(day1);
        }else{
            day= String.valueOf(day1);
        }
        dateh.setText(year+"/"+month+"/"+day);

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sugar1=sugar.getText().toString();
                String sugar2=sugar_random.getText().toString();
                String bp1=bp_d.getText().toString();
                String bp2=bp_s.getText().toString();
                String insulin1=insulin.getText().toString();




                String food1 = sp.getSelectedItem().toString();
                String date1=dateh.getText().toString();
                        //date.getText().toString(); // yyyy/mm/dd


                if(!validate(sugar1,sugar2,bp1,bp2,insulin1,food1,date1))
                {
                    Toast.makeText(Secondary_req.this, "enter valid details",
                            Toast.LENGTH_SHORT).show();
                }else {
                    String userid = mAuth.getCurrentUser().getUid();

                    Secondaryreq_util user = new Secondaryreq_util(sugar1,sugar2, bp1,bp2, insulin1, food1);

                    mDatabase.child(userid).child("Secondary_requirements").child(date1).setValue(user);


                    Intent in = new Intent(Secondary_req.this, Homescreen.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                }


            }
        });



    }

    private boolean validate(String sugar1,String sugar2,String bp1,String bp2,String insulin1,String food1,String date1)
    {
        boolean m1=true;


        if(sugar1.isEmpty()||!(Double.parseDouble(sugar1)>0 && (Double.parseDouble(sugar1)<500)))
        {

            s="Your fasting Sugar Level input is invalid";
            sugar.setError(s);
            m1=false;
        }else {
            sugar.setError(null);
        }

        if(sugar2.isEmpty()||!(Double.parseDouble(sugar2)>0 && (Double.parseDouble(sugar2)<500)))
        {

            s="Your after food Sugar Level input is invalid";
            sugar_random.setError(s);
            m1=false;
        }else {
            sugar_random.setError(null);
        }



        if(bp1.isEmpty()|| !(Integer.parseInt(bp1)>40 && Integer.parseInt(bp1)<100))
        {

            s="Your Diastolic input should be between 40 to 100";
            bp_d.setError(s);
            m1=false;
        }else {
            bp_d.setError(null);
        }

        if(bp2.isEmpty()|| !(Integer.parseInt(bp2)>60 && Integer.parseInt(bp2)<190))
        {

            s="Your systolic input should be between 60 to 190";
            bp_s.setError(s);
            m1=false;
        }else {
            bp_s.setError(null);
        }

        if(insulin1.isEmpty()|| !(Integer.parseInt(insulin1)>0 && Integer.parseInt(insulin1)<20))
        {

            s="Insulin Data Invalid. Put 0 units if you have not taken your Insulin today";
            insulin.setError(s);
            m1=false;
        }else {
            insulin.setError(null);
        }

       /* if(food1.isEmpty())
        {

            s="Enter Food Detais";
            food.setError(s);
            m1=false;
        }else {
            food.setError(null);
        }
        */




        return m1;
    }

}
