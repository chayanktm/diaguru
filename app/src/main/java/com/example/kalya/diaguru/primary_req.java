package com.example.kalya.diaguru;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class primary_req extends AppCompatActivity {
    private String s ="";
    private EditText name,age,dob,occupation,address,height,weight,bmi,dodiabetes,hba1c,phone;
    private RadioGroup tod,tot,gender;
    private RadioButton gen,tod1,tot1;
    private Button submit;
    private DatabaseReference mDatabase,mAdminDatabase;
    private FirebaseAuth mAuth;


    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (mHandler != null) { mHandler.removeCallbacks(mRunnable); }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            deleteUser();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "on clicking BACK again to exit , you will be deleting your account from diaguru", Toast.LENGTH_LONG).show();

        mHandler.postDelayed(mRunnable, 2000);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary_req);

        //below all are edit text
       name=(EditText) findViewById(R.id.edtxt_name);
        age=(EditText) findViewById(R.id.edtxt_age);
        dob=(EditText) findViewById(R.id.edtxt_dob);
        occupation=(EditText) findViewById(R.id.edtxt_occupation);
        address=(EditText) findViewById(R.id.edtxt_address);
        height=(EditText) findViewById(R.id.edtxt_height);
        weight=(EditText) findViewById(R.id.edtxt_weight);
        bmi=(EditText) findViewById(R.id.edtxt_bmi);
        dodiabetes=(EditText) findViewById(R.id.edtxt_durationOfDiabtetes);
        hba1c=(EditText) findViewById(R.id.edtxt_hba1c);
        phone=(EditText) findViewById(R.id.edtxt_phone);

        // below all are radio group and their respective radiobuttons
        tod=(RadioGroup) findViewById(R.id.rgbtn_typeofdiabetes);
        tot=(RadioGroup) findViewById(R.id.rgbtn_typeoftreatment);
        gender=(RadioGroup) findViewById(R.id.rgbtn_gender);

        //firebase
        mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
        mAdminDatabase=FirebaseDatabase.getInstance().getReference().child("Doctor").child("Users");
        mAuth = FirebaseAuth.getInstance();


        //button
        submit=(Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// get selected radio button from radioGroups
                int gendercheck=-1,todcheck=-1,totcheck=-1;
                String tod2="",tot2="",gender1="";
                boolean check=true;
                if (gender.getCheckedRadioButtonId() == -1)
                {
                   check=false;

                }
                else
                {
                  gendercheck = gender.getCheckedRadioButtonId();
                    gen=(RadioButton) findViewById(gendercheck);
                    gender1=gen.getText().toString();
                }

                if (tod.getCheckedRadioButtonId() == -1)
                {
                    check=false;
                }
                else
                {
                    // one of the radio buttons is checked
                    todcheck = tod.getCheckedRadioButtonId();
                    tod1=(RadioButton) findViewById(todcheck);
                    tod2=tod1.getText().toString();
                }

                if (tot.getCheckedRadioButtonId() == -1)
                {
                    check=false;
                }
                else
                {
                    totcheck = tot.getCheckedRadioButtonId();
                    tot1=(RadioButton) findViewById(totcheck);
                    tot2=tot1.getText().toString();
                }


                ////// find the radiobutton by returned id




                //// assign to string
                String name1=name.getText().toString();
                String age1=age.getText().toString();

                String dob1=dob.getText().toString();
                String occupation1=occupation.getText().toString();
                String address1=address.getText().toString();
                String height1=height.getText().toString();
                String weight1=weight.getText().toString();
                String bmi1=bmi.getText().toString();

                String dod=dodiabetes.getText().toString();

                String hb1ac1=hba1c.getText().toString();
                String phone1=phone.getText().toString();
                //userid


                if(!validate(name1,age1,dob1,occupation1,address1,height1,weight1,bmi1,dod,hb1ac1,phone1) || gender1.isEmpty() || tod2.isEmpty() || tot2.isEmpty() )
                {
                    Toast.makeText(primary_req.this, "enter valid details & select appropriate radio buttons",
                            Toast.LENGTH_SHORT).show();
                }else {
                    String userid=mAuth.getCurrentUser().getUid();

                    primaryreq_util user=new primaryreq_util(name1,age1,gender1,dob1,occupation1,address1,height1,weight1,bmi1,tod2,dod,tot2,hb1ac1,phone1);
                    mDatabase.child(userid).child("Primary_Requirements").setValue(user);

                    mAdminDatabase.child(userid).child("email").setValue(mAuth.getCurrentUser().getEmail());
                    mAdminDatabase.child(userid).child("phone number").setValue(user.mobile_number);
                    mAdminDatabase.child(userid).child("name").setValue(user.fullname);

                    Intent in = new Intent(primary_req.this, Homescreen.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                }

            }
        });

    }
    private void deleteUser(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("USER", "User account deleted.");
                        }
                    }
                });
    }


    private boolean validate(String name1,String age1,String dob1,String occupation1,String address1,String height1,String weight1,String bmi1,String dodiabetes1,String hb1ac11,String phone1)
    {

        boolean n1=true;




        if(name1.isEmpty())
        {

           s="Input a valid NAME";
            name.setError(s);
            n1=false;
        }else {
            name.setError(null);
        }

        if(age1.isEmpty()||!(Integer.parseInt(age1)>0 && Integer.parseInt(age1)<100))
        {
           s="Input a valid age";
            age.setError(s);
            n1=false;
        }else {
            age.setError(null);
        }

        if(dob1.isEmpty()||!dob1.matches("^\\d{4}/[01]\\d/[0-3]\\d"))
        {
           s="Input a valid format, please follow this format - Date(yyyy/mm/dd)";
            dob.setError(s);
            n1=false;
        }else {
            dob.setError(null);
        }

        if(occupation1.isEmpty())
        {
            s="Input a valid Occupation details";
            occupation.setError(s);
            n1=false;
        }else {
            occupation.setError(null);
        }

        if(address1.isEmpty()||!address1.matches("^[#.0-9a-zA-Z\\s,-]+"))
        {
            s="ADDRESS is Invalid";
            address.setError(s);
            n1=false;
        }else {
            address.setError(null);
        }

        if(height1.isEmpty()||!(Double.parseDouble(height1)>2 && Double.parseDouble(height1)<8))
        {
            s="Input a valid HEIGHT in ft";
            height.setError(s);
            n1=false;
        }else {
            height.setError(null);
        }

        if(weight1.isEmpty()||!(Integer.parseInt(weight1)>0 && Integer.parseInt(weight1)<1000))
        {
            s="Input a valid WEIGHT in KG";
            weight.setError(s);
            n1=false;
        }else {
            weight.setError(null);
        }

        if(bmi1.isEmpty()||!((Double.parseDouble(bmi1) > 0 && Double.parseDouble(bmi1)<50)))
        {
           s="Your BMI is Invalid,it should be between 1 & 50";
            bmi.setError(s);
            n1=false;
        }else {
            bmi.setError(null);
        }

        if(dodiabetes1.isEmpty()||!dodiabetes1.matches("^\\d{4}/[01]\\d/[0-3]\\d"))
        {
            s="Date of Diabeties is Invalid";
            dodiabetes.setError(s);
            n1=false;
        }else {
            dodiabetes.setError(null);
        }

        if(hb1ac11.isEmpty()||!(Double.parseDouble(hb1ac11) > 0 &&  Double.parseDouble(hb1ac11)< 10))
        {
            s="hb1ac is invalid ,it should be between 1 & 10";
            hba1c.setError(s);
            n1=false;
        }else {
            hba1c.setError(null);
        }

        if(phone1.isEmpty()||!phone1.matches("^\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}"))
        {
            s="Input a valid phone number";
            phone.setError(s);
            n1=false;
        }else {
            phone.setError(null);
        }

        return n1;

    }
}
