package com.example.kalya.diaguru;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class forgot_password extends AppCompatActivity {
    private EditText email;
    private Button send;
    private FirebaseAuth mAuth;
    private ProgressDialog mprogressdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        mAuth = FirebaseAuth.getInstance();

        mprogressdialog =new ProgressDialog(this);


        email=(EditText) findViewById(R.id.edtxt_resetpassword);
        send=(Button) findViewById(R.id.btn_resetpassword);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailAddress = email.getText().toString();

                mprogressdialog.setMessage(" Sending mail to "+emailAddress);
                mprogressdialog.show();

                boolean valid = true;


                if (TextUtils.isEmpty(emailAddress)) {
                    email.setError("Required.");
                    valid = false;
                } else {
                    email.setError(null);
                }

                if(!valid){
                    Toast.makeText(forgot_password.this, "please enter valid email id", Toast.LENGTH_LONG).show();
                    mprogressdialog.dismiss();
                    return;
                }

                mAuth.sendPasswordResetEmail(emailAddress)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("hey", "Email sent.");
                                    mprogressdialog.dismiss();
                                    Toast.makeText(forgot_password.this, "Email has been sent.please reset password and signin. :)", Toast.LENGTH_LONG).show();
                                    Intent mainIntent=new Intent(forgot_password.this,Signin.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(mainIntent);

                                }else{
                                    mprogressdialog.dismiss();
                                    Toast.makeText(forgot_password.this, "Email has not been sent.maybe due to incorrect email id. :(", Toast.LENGTH_LONG).show();

                                }
                            }
                        });
            }
        });




    }
}
