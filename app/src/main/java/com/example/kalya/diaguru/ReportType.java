package com.example.kalya.diaguru;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReportType extends AppCompatActivity {

    Button diabetes,bp,insulin,foodintake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_type);

        diabetes=(Button) findViewById(R.id.btn_diabetes);
        diabetes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(ReportType.this,DiabetesGraph.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);

            }
        });

        bp=(Button) findViewById(R.id.btn_bp);
        bp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(ReportType.this,BpGraph.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);
            }
        });

        insulin=(Button) findViewById(R.id.btn_insulin);
        insulin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(ReportType.this,InsulinGraph.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);
            }
        });

    /*
        foodintake=(Button) findViewById(R.id.btn_foodintake);
        foodintake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out=new Intent(ReportType.this,FoodIntake.class);
                out.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(out);
            }
        });
        */


    }
}
