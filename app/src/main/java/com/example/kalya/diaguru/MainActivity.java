package com.example.kalya.diaguru;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import static com.example.kalya.diaguru.NetworkCheck.isNetworkAvailable;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUsers;
    private BroadcastReceiver broadcastReceiver;

    private ProgressDialog mprogressdialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        mprogressdialog =new ProgressDialog(this);
        mDatabaseUsers= FirebaseDatabase.getInstance().getReference().child("Users");
        broadcastReceiver =new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
        //setText(SharedPrefManager.getInstance(MainActivity.this).getToken();
            }
        };

   //     if(SharedPrefManager.getInstance(this).getToken() != null)
            //setText(SharedPrefManager.getInstance(MainActivity.this).getToken();

        registerReceiver(broadcastReceiver,new IntentFilter(MyFirebaseInstanceIDService.TOKEN_BROADCAST));

    }


    @Override
    public void onStart() {
        super.onStart();


        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"No Internet connection",Toast.LENGTH_LONG).show();
            finish(); //Calling this method to close this activity when internet is not available.
        }
       // mprogressdialog.setMessage(" Loading...");
       // mprogressdialog.show();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser= mAuth.getCurrentUser();
       checkuserexist(currentUser);
    }

    private void checkuserexist(FirebaseUser user) {

        if (user != null) {
            final String user_id=user.getUid();
            if(user.getEmail().equals("doc@gmail.com")){
               // mprogressdialog.dismiss();
                Intent in = new Intent(MainActivity.this, DocHomeScreen.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
            }else {
                mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(user_id)) {

                           // mprogressdialog.dismiss();

                            Intent in = new Intent(MainActivity.this, Homescreen.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(in);
                            finish();

                        } else {

                          //  mprogressdialog.dismiss();
                            Intent in = new Intent(MainActivity.this, Signin.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(in);
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }


        } else {

          //  mprogressdialog.dismiss();

            Intent in = new Intent(MainActivity.this,Signin.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
            finish();


        }
    }


}
